package com.example.database

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class User(
    @field:PrimaryKey(autoGenerate = true)
    @field:ColumnInfo(name = "id") val id: Int,
    @field:ColumnInfo(name = "name") val name: String,
    @field:ColumnInfo(name = "username") val username: String,
    @field:ColumnInfo(name = "email") val email: String,
    @field:ColumnInfo(name = "phone") val phone: String,
    @field:ColumnInfo(name = "website") val website: String,
)