package com.example.network

import com.example.network.response.UserResponse
import retrofit2.http.GET
import retrofit2.http.Path

interface AppApi {

    @GET("users")
    suspend fun getUsers(): List<UserResponse>

    @GET("users/{id}")
    suspend fun getUser(@Path("id") id: Int): UserResponse?
}