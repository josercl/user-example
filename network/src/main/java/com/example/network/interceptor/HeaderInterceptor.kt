package com.example.network.interceptor

import okhttp3.Interceptor
import okhttp3.Response

class HeaderInterceptor(val tokenProvider: () -> String): Interceptor {
    override fun intercept(chain: Interceptor.Chain): Response {
        val token = tokenProvider()
        val origRequest = chain.request()

        val request = origRequest
            .newBuilder()
            .apply {
                addHeader("Content-Type", "application/json")
            }

        if (token.isNotEmpty()) {
            request.addHeader("Authorization", "Bearer $token")
        }

        return chain.proceed(request.build())
    }
}