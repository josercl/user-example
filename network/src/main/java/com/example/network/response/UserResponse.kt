package com.example.network.response


import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class UserResponse(
    @field:Json(name = "id") val id: Int = 0,
    @field:Json(name = "name") val name: String = "",
    @field:Json(name = "username") val username: String = "",
    @field:Json(name = "email") val email: String = "",
    @field:Json(name = "address") val address: AddressResponse = AddressResponse(),
    @field:Json(name = "phone") val phone: String = "",
    @field:Json(name = "website") val website: String = "",
    @field:Json(name = "company") val company: CompanyResponse = CompanyResponse()
)