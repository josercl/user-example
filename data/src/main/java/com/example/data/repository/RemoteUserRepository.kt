package com.example.data.repository

import com.example.domain.repository.RemoteRepository
import com.example.network.AppApi
import com.example.network.response.UserResponse

class RemoteUserRepository(
    private val api: AppApi
) : RemoteRepository<UserResponse, Int> {

    override suspend fun getList(): List<UserResponse> {
        return api.getUsers()
    }

    override suspend fun find(id: Int): UserResponse? {
        return api.getUser(id)
    }

    override suspend fun save(vararg item: UserResponse) {
        //noop
    }
}
