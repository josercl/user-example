package com.example.data.repository

import com.example.domain.repository.LocalRepository
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow

@ExperimentalCoroutinesApi
class LocalUserRepository(
    private val userDao: com.example.database.dao.UserDao
) : LocalRepository<com.example.database.User, Int> {

    override fun getList(): Flow<List<com.example.database.User>> {
        return userDao.getUsers()
    }

    override suspend fun count(): Int = userDao.count()

    override suspend fun find(id: Int): com.example.database.User? {
        return userDao.getUser(id)
    }

    override suspend fun save(vararg item: com.example.database.User) {
        return userDao.save(*item)
    }
}