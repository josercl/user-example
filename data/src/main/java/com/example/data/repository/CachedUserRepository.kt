package com.example.data.repository

import com.example.data.mapper.toDBUser
import com.example.data.mapper.toDomainUser
import com.example.domain.User
import com.example.domain.repository.LocalRepository
import com.example.domain.repository.RemoteRepository
import com.example.network.response.UserResponse
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.emitAll
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.mapLatest
import com.example.database.User as DBUser

@ExperimentalCoroutinesApi
class CachedUserRepository(
    private val localUserRepository: LocalRepository<DBUser, Int>,
    private val remoteUserRepository: RemoteRepository<UserResponse, Int>,
) : LocalRepository<User, Int> {

    override fun getList(): Flow<List<User>> {
        return flow {
            if (localUserRepository.count() == 0) {
                val remoteUsers = remoteUserRepository.getList()

                val newUsers = remoteUsers.map {
                    DBUser(
                        it.id,
                        it.name,
                        it.username,
                        it.email,
                        it.phone,
                        it.website
                    )
                }.toTypedArray()

                localUserRepository.save(*newUsers)
            }

            emitAll(localUserRepository.getList().mapLatest { list -> list.map{it.toDomainUser()} })
        }
    }

    override suspend fun count(): Int = localUserRepository.count()

    override suspend fun find(id: Int): User {
        return localUserRepository.find(id)?.run {
            this.toDomainUser()
        } ?: run {
            val userResponse = remoteUserRepository.find(id) ?: throw RuntimeException("Not found")

            localUserRepository.save(userResponse.toDBUser())

            localUserRepository.find(id)!!.toDomainUser()
        }
    }

    override suspend fun save(vararg item: User) {

    }
}