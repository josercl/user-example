package com.example.data.mapper

import com.example.domain.Address
import com.example.domain.Company
import com.example.network.response.AddressResponse
import com.example.network.response.CompanyResponse
import com.example.network.response.UserResponse
import com.example.database.User as DBUser
import com.example.domain.User as DomainUser

fun UserResponse.toDomainUser() = DomainUser(
    id,
    name,
    username,
    email,
    address.toAddress(),
    phone,
    website,
    company.toCompany()
)

fun UserResponse.toDBUser() = DBUser(
    id,
    name,
    username,
    email,
    phone,
    website,
)

fun AddressResponse.toAddress() = Address(
    street,
    suite,
    city,
    zipcode,
    "${geo.lat}, ${geo.lng}"
)

fun CompanyResponse.toCompany() = Company(name, catchPhrase, bs)

fun com.example.database.User.toDomainUser() = DomainUser(
    id, name, username, email, Address(), phone, website, Company()
)