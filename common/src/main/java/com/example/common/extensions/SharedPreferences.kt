package com.example.common.extensions

import android.content.SharedPreferences
import androidx.core.content.edit

fun SharedPreferences.setToken(token: String) = apply {
    edit(true) {
        putString("SAVED_TOKEN", token)
    }
}

fun SharedPreferences.getToken() = getString("SAVED_TOKEN", "")!!