package com.example.common.extensions

import android.content.Context
import android.content.Intent
import android.net.Uri

fun Context.startPhoneCall(phone: String) {
    startActivity(
        Intent(
            Intent.ACTION_DIAL,
            Uri.parse("tel:$phone")
        )
    )
}