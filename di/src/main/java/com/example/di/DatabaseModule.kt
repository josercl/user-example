package com.example.di

import android.content.Context
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object DatabaseModule {
    @Provides
    @Singleton
    fun providesDatabase(@ApplicationContext context: Context): com.example.database.AppDatabase {
        return com.example.database.AppDatabase.getInstance(context)
    }

    @Provides
    @Singleton
    fun providesUserDao(appDatabase: com.example.database.AppDatabase): com.example.database.dao.UserDao = appDatabase.userDao()
}