package com.example.di

import android.content.SharedPreferences
import com.example.common.extensions.getToken
import com.example.network.AppApi
import com.example.network.interceptor.HeaderInterceptor
import com.squareup.moshi.Moshi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import retrofit2.converter.scalars.ScalarsConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object NetworkModule {
    @Singleton
    @Provides
    fun providesMoshi(): Moshi = Moshi.Builder().build()

    @Singleton
    @Provides
    fun providesHeaderInterceptor(preferences: SharedPreferences): HeaderInterceptor =
        HeaderInterceptor(preferences::getToken)

    @Singleton
    @Provides
    fun providesHttpClient(
        headersInterceptor: HeaderInterceptor
    ): OkHttpClient = OkHttpClient.Builder()
        .readTimeout(0, TimeUnit.MILLISECONDS)
        .retryOnConnectionFailure(true)
        .addNetworkInterceptor(headersInterceptor)
        .addNetworkInterceptor(HttpLoggingInterceptor().apply {
            setLevel(HttpLoggingInterceptor.Level.BASIC)
        })
        .build()


    @Singleton
    @Provides
    fun provideAppApi(
        moshi: Moshi,
        httpClient: OkHttpClient,
    ) : AppApi {
        return Retrofit.Builder()
            .baseUrl("https://jsonplaceholder.typicode.com/")
            .client(httpClient)
            .addConverterFactory(MoshiConverterFactory.create(moshi))
            .addConverterFactory(ScalarsConverterFactory.create())
            .build()
            .create(AppApi::class.java)
    }
}