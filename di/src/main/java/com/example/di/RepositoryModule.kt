package com.example.di

import com.example.data.repository.CachedUserRepository
import com.example.data.repository.LocalUserRepository
import com.example.data.repository.RemoteUserRepository
import com.example.domain.repository.LocalRepository
import com.example.domain.repository.RemoteRepository
import com.example.network.AppApi
import com.example.network.response.UserResponse
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import kotlinx.coroutines.ExperimentalCoroutinesApi
import javax.inject.Singleton
import com.example.database.User as DBUser
import com.example.domain.User as DomainUser

@ExperimentalCoroutinesApi
@Module
@InstallIn(SingletonComponent::class)
object RepositoryModule {
    @Provides
    @Singleton
    fun providesRemoteUserRepository(api: AppApi): RemoteRepository<UserResponse, Int> = RemoteUserRepository(api)

    @Provides
    @Singleton
    fun providesLocalUserRepository(userDao: com.example.database.dao.UserDao): LocalRepository<DBUser, Int> = LocalUserRepository(userDao)

    @Provides
    @Singleton
    fun providesUserRepository(
        remoteRepository: RemoteRepository<UserResponse, Int>,
        localRepository: LocalRepository<DBUser, Int>
    ): LocalRepository<DomainUser, Int> = CachedUserRepository(localRepository, remoteRepository)
}