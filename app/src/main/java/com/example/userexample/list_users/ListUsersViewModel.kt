package com.example.userexample.list_users

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.UIState
import com.example.domain.User
import com.example.domain.repository.LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ListUsersViewModel @Inject constructor(
    private val userRepository: LocalRepository<User, Int>,
) : ViewModel() {
    val state = MutableStateFlow<UIState<List<User>>>(UIState.Idle)

    init {
        viewModelScope.launch {
            state.update {
                UIState.Loading
            }
            runCatching {
                userRepository.getList()
                    .collectLatest { list ->
                        state.update {
                            UIState.Success(list)
                        }
                    }
            }.onFailure { error ->
                Timber.e(error)
                state.update {
                    UIState.Error(error)
                }
            }
        }
    }
}