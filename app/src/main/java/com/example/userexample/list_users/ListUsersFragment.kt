package com.example.userexample.list_users

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.example.common.UIState
import com.example.domain.User
import com.example.userexample.R
import com.example.userexample.databinding.FragmentListUsersBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class ListUsersFragment: Fragment(R.layout.fragment_list_users) {
    private lateinit var binding: FragmentListUsersBinding

    private val viewModel: ListUsersViewModel by viewModels()

    private val listAdapter = UserAdapter {
        findNavController().navigate(
            ListUsersFragmentDirections.toUserDetails(it.id)
        )
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding = FragmentListUsersBinding.bind(view)

        binding.userList.adapter = listAdapter
        binding.userList.addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL))

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.state.collectLatest {
                    updateUI(it)
                }
            }
        }
    }

    private fun updateUI(state: UIState<List<User>>) {
        binding.loadingIndicator.isVisible = state is UIState.Loading

        if (state is UIState.Success) {
            listAdapter.submitList(state.data)
        }

        if (state is UIState.Error) {
            Snackbar.make(
                binding.root,
                state.error.localizedMessage ?: state.error.message ?: "Error",
                Snackbar.LENGTH_LONG
            ).show()
        }
    }
}