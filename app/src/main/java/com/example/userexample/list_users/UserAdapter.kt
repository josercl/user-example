package com.example.userexample.list_users

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.example.domain.User
import com.example.userexample.databinding.ItemSimpleUserBinding

class UserAdapter(
    private val onClick: (User) -> Unit
): ListAdapter<User, UserViewHolder>(object: DiffUtil.ItemCallback<User>() {
    override fun areItemsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: User, newItem: User): Boolean {
        return oldItem == newItem
    }
}) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): UserViewHolder {
        return UserViewHolder(
            ItemSimpleUserBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            ),
            onClick
        )
    }

    override fun onBindViewHolder(holder: UserViewHolder, position: Int) {
        holder.bindTo(getItem(position))
    }
}

class UserViewHolder(
    private val binding: ItemSimpleUserBinding,
    private val onClick: (User) -> Unit
): RecyclerView.ViewHolder(binding.root){

    private var item: User? = null

    init {
        itemView.setOnClickListener {
            item?.run {
                onClick(this@run)
            }
        }
    }

    fun bindTo(item: User) {
        this.item = item

        binding.userId.text = item.id.toString().padStart(2, '0')
        binding.userName.text = item.name
        binding.userEmail.text = item.email
    }
}