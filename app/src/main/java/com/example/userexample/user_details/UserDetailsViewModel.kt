package com.example.userexample.user_details

import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.UIState
import com.example.domain.User
import com.example.domain.repository.LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.update
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor(
    private val userRepository: LocalRepository<User, Int>,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    val state = MutableStateFlow<UIState<User>>(UIState.Idle)

    init {
        viewModelScope.launch {
            state.update { UIState.Loading }
            val id: Int = savedStateHandle.get<Int>("userId") ?: run {
                state.update {
                    UIState.Error(RuntimeException("No userId"))
                }
                return@launch
            }

            runCatching {
                val data = userRepository.find(id) ?: throw RuntimeException("Not found")
                state.update {
                    UIState.Success(data = data)
                }
            }.onFailure { error ->
                Timber.e(error)
                state.update {
                    UIState.Error(error)
                }
            }
        }
    }
}