package com.example.userexample.user_details

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import com.example.common.UIState
import com.example.common.extensions.startPhoneCall
import com.example.domain.User
import com.example.userexample.R
import com.example.userexample.databinding.FragmentUserDetailsBinding
import com.google.android.material.snackbar.Snackbar
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch

@AndroidEntryPoint
class UserDetailsFragment: Fragment(R.layout.fragment_user_details) {

    private val viewModel: UserDetailsViewModel by viewModels()

    private lateinit var binding: FragmentUserDetailsBinding

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding = FragmentUserDetailsBinding.bind(view)

        binding.lifecycleOwner = viewLifecycleOwner

        binding.phone.setOnClickListener {
            requireContext().startPhoneCall(binding.phone.text.toString())
        }

        viewLifecycleOwner.lifecycleScope.launch {
            viewLifecycleOwner.repeatOnLifecycle(Lifecycle.State.RESUMED) {
                viewModel.state.collectLatest(this@UserDetailsFragment::updateUI)
            }
        }
    }

    private fun updateUI(state: UIState<User>) {
        binding.loading = state is UIState.Loading

        if (state is UIState.Success) {
            binding.user = state.data
        }

        if (state is UIState.Error) {
            Snackbar.make(
                binding.root,
                state.error.localizedMessage ?: state.error.message ?: "Error",
                Snackbar.LENGTH_LONG
            ).show()
        }
    }
}