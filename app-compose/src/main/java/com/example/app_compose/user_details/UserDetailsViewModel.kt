package com.example.app_compose.user_details

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.SavedStateHandle
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.UIState
import com.example.domain.User
import com.example.domain.repository.LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class UserDetailsViewModel @Inject constructor(
    private val userRepository: LocalRepository<User, Int>,
    private val savedStateHandle: SavedStateHandle
) : ViewModel() {
    var state by mutableStateOf<UIState<User>>(UIState.Idle)

    init {
        viewModelScope.launch {
            state = UIState.Loading
            val id: Int = savedStateHandle.get<Int>("userId") ?: run {
                state = UIState.Error(RuntimeException("No userId"))
                return@launch
            }

            runCatching {
                val data: User = userRepository.find(id) ?: throw RuntimeException("Not found")
                state = UIState.Success(data = data)
            }.onFailure { error ->
                Timber.e(error)
                state = UIState.Error(error)
            }
        }
    }
}