package com.example.app_compose.user_details

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.platform.LocalContext
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.app_compose.R
import com.example.app_compose.ui.theme.UserExampleTheme
import com.example.common.UIState
import com.example.common.extensions.startPhoneCall
import com.example.domain.Address
import com.example.domain.Company
import com.example.domain.User

@Composable
fun UserDetailsScreen(
    state: UIState<User>
) {
    val context = LocalContext.current

    Box(
        modifier = Modifier
            .fillMaxSize()
            .padding(16.dp)
    ) {
        if (state is UIState.Success) {
            val user = state.data
            Column(
                modifier = Modifier.fillMaxWidth(),
                verticalArrangement = Arrangement.spacedBy(8.dp)
            ) {
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.details_name_label),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = user.name,
                        modifier = Modifier.weight(1f),
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.details_username_label),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = user.username,
                        modifier = Modifier.weight(1f),
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.details_email_label),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = user.email,
                        modifier = Modifier.weight(1f),
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                    Text(
                        text = stringResource(id = R.string.details_phone_label),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Text(
                        text = user.phone,
                        modifier = Modifier.weight(1f)
                            .clickable {
                                context.startPhoneCall(user.phone)
                            },
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(), horizontalArrangement = Arrangement.spacedBy(8.dp)) {
                    Text(
                        modifier = Modifier.padding(bottom = MaterialTheme.typography.bodyMedium.fontSize.value.dp),
                        text = stringResource(id = R.string.details_address_label),
                        style = MaterialTheme.typography.titleMedium,
                    )
                    Text(
                        text = user.address.toString(),
                        modifier = Modifier.weight(1f),
                    )
                }
                Row(
                    modifier = Modifier.fillMaxWidth(),
                    horizontalArrangement = Arrangement.spacedBy(8.dp)
                ) {
                    Text(
                        text = stringResource(id = R.string.details_company_label),
                        style = MaterialTheme.typography.titleMedium
                    )
                    Column(
                        modifier = Modifier.weight(1f),
                        verticalArrangement = Arrangement.spacedBy(8.dp),
                    ) {
                        Text(text = user.company.name)
                        Text(text = user.company.catchPhrase)
                        Text(text = user.company.bs)
                    }
                }
            }
        }
        if (state is UIState.Loading) {
            CircularProgressIndicator(
                modifier = Modifier
                    .align(Alignment.TopCenter)
                    .padding(top = 32.dp)
            )
        }
    }
}

@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun UserDetailsScreenPreview() {
    UserExampleTheme {
        Surface {
            UserDetailsScreen(
                UIState.Success(
                    User(
                        0,
                        "Pedro Perez",
                        "pedrito",
                        "email@example.com",
                        Address(
                            "street",
                            "suite",
                            "City",
                            "123123",
                            "lat, lon"
                        ),
                        "123123123",
                        "https://www.google.com",
                        Company(
                            "Super duper company",
                            "We're super duper",
                            "bs"
                        )
                    )
                )
            )
        }
    }
}