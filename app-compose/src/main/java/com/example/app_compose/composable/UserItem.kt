package com.example.app_compose.composable

import android.content.res.Configuration
import androidx.compose.foundation.clickable
import androidx.compose.foundation.layout.Arrangement
import androidx.compose.foundation.layout.Column
import androidx.compose.foundation.layout.Row
import androidx.compose.foundation.layout.fillMaxWidth
import androidx.compose.foundation.layout.padding
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.material3.Text
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.app_compose.ui.theme.UserExampleTheme

@Composable
fun UserItem(
    userId: Int,
    userName: String,
    userEmail: String,
    onClick: () -> Unit = {},
) {
    Row(
        modifier = Modifier
            .fillMaxWidth()
            .clickable { onClick() }
            .padding(16.dp),
        horizontalArrangement = Arrangement.spacedBy(16.dp),
        verticalAlignment = Alignment.CenterVertically,
    ) {
        Text(
            text = userId.toString().padStart(2, '0')
        )
        Column(
            modifier = Modifier.weight(1f)
        ) {
            Text(text = userName, style = MaterialTheme.typography.bodyMedium)
            Text(text = userEmail, style = MaterialTheme.typography.bodySmall)
        }
    }
}

@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun UserItemPreview() {
    UserExampleTheme {
        Surface {
            UserItem(
                userId = 1,
                userName = "Jose Carrero",
                userEmail = "jose@grupoapok.com"
            )
        }
    }
}