package com.example.app_compose

import androidx.annotation.StringRes

enum class Routes(val route: String, @StringRes val titleResource: Int) {
    LIST("list", titleResource = R.string.list_title),
    DETAILS("details/{userId}", titleResource = R.string.details_title)
}