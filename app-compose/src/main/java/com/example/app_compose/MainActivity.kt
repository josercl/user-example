package com.example.app_compose

import android.os.Bundle
import androidx.activity.ComponentActivity
import androidx.activity.compose.setContent
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.layout.padding
import androidx.compose.material.icons.Icons
import androidx.compose.material.icons.filled.ArrowBack
import androidx.compose.material3.ExperimentalMaterial3Api
import androidx.compose.material3.Icon
import androidx.compose.material3.IconButton
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Scaffold
import androidx.compose.material3.SmallTopAppBar
import androidx.compose.material3.Text
import androidx.compose.material3.TopAppBarDefaults
import androidx.compose.runtime.SideEffect
import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.saveable.rememberSaveable
import androidx.compose.runtime.setValue
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.res.stringResource
import androidx.compose.ui.unit.dp
import androidx.hilt.navigation.compose.hiltViewModel
import androidx.navigation.NavType
import androidx.navigation.compose.NavHost
import androidx.navigation.compose.composable
import androidx.navigation.compose.rememberNavController
import androidx.navigation.navArgument
import com.example.app_compose.list_users.ListUsersScreen
import com.example.app_compose.list_users.ListUsersViewModel
import com.example.app_compose.ui.theme.UserExampleTheme
import com.example.app_compose.user_details.UserDetailsScreen
import com.example.app_compose.user_details.UserDetailsViewModel
import dagger.hilt.android.AndroidEntryPoint

@ExperimentalMaterial3Api
@AndroidEntryPoint
class MainActivity : ComponentActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContent {
            UserExampleTheme {
                val navController = rememberNavController()

                var showBackArrow by rememberSaveable { mutableStateOf(false) }

                var title by rememberSaveable { mutableStateOf(0) }

                SideEffect {
                    navController.addOnDestinationChangedListener { _, destination, _ ->
                        showBackArrow = destination.route != Routes.LIST.route
                        title = when (destination.route) {
                            Routes.LIST.route -> Routes.LIST.titleResource
                            Routes.DETAILS.route -> Routes.DETAILS.titleResource
                            else -> 0
                        }
                    }
                }

                Scaffold(
                    topBar = {
                        SmallTopAppBar(
                            title = {
                                Text(text = if (title == 0) "" else stringResource(id = title))
                            },
                            navigationIcon = {
                                if (showBackArrow) {
                                    IconButton(onClick = { navController.popBackStack() }) {
                                        Icon(imageVector = Icons.Default.ArrowBack, contentDescription = null)
                                    }
                                }
                            },
                            colors = TopAppBarDefaults.smallTopAppBarColors(
                                containerColor = MaterialTheme.colorScheme.primary,
                                titleContentColor = MaterialTheme.colorScheme.onPrimary,
                                navigationIconContentColor = MaterialTheme.colorScheme.onPrimary
                            )
                        )
                    },
                ) { padding ->
                    Box(modifier = Modifier.fillMaxSize()) {
                        NavHost(
                            navController = navController,
                            startDestination = "list",
                            modifier = Modifier.padding(padding),
                        ) {
                            composable(Routes.LIST.route) {
                                val viewModel: ListUsersViewModel = hiltViewModel()

                                ListUsersScreen(
                                    state = viewModel.state,
                                    onUserClicked = {
                                        navController.navigate("details/${it.id}")
                                    }
                                )
                            }

                            composable(
                                route = Routes.DETAILS.route,
                                arguments = listOf(
                                    navArgument("userId") {
                                        type = NavType.IntType
                                        nullable = false
                                    }
                                )
                            ) {
                                val viewModel: UserDetailsViewModel = hiltViewModel()

                                UserDetailsScreen(viewModel.state)
                            }
                        }
                        Text(text = "Compose", modifier = Modifier
                            .align(Alignment.BottomEnd)
                            .padding(16.dp)
                        )
                    }
                }
            }
        }
    }
}