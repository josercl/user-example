package com.example.app_compose.list_users

import android.content.res.Configuration
import androidx.compose.foundation.layout.Box
import androidx.compose.foundation.layout.fillMaxSize
import androidx.compose.foundation.lazy.LazyColumn
import androidx.compose.foundation.lazy.items
import androidx.compose.material3.CircularProgressIndicator
import androidx.compose.material3.Divider
import androidx.compose.material3.MaterialTheme
import androidx.compose.material3.Surface
import androidx.compose.runtime.Composable
import androidx.compose.ui.Alignment
import androidx.compose.ui.Modifier
import androidx.compose.ui.tooling.preview.Preview
import androidx.compose.ui.unit.dp
import com.example.app_compose.composable.UserItem
import com.example.app_compose.ui.theme.UserExampleTheme
import com.example.common.UIState
import com.example.domain.User

@Composable
fun ListUsersScreen(
    state: UIState<List<User>>,
    onUserClicked: (User) -> Unit = {}
) {
    Box(
        modifier = Modifier.fillMaxSize(),
    ) {
        if (state is UIState.Success) {
            LazyColumn {
                items(state.data) { item: User ->
                    UserItem(
                        userId = item.id,
                        userName = item.name,
                        userEmail = item.email,
                    ) { onUserClicked(item) }
                    Divider(
                        color = MaterialTheme.colorScheme.outline,
                        thickness = .5.dp,
                    )
                }
            }
        }

        if (state is UIState.Loading) {
            CircularProgressIndicator(
                modifier = Modifier.align(Alignment.Center)
            )
        }
    }
}

@Preview(group = "Light")
@Preview(uiMode = Configuration.UI_MODE_NIGHT_YES, group = "Dark")
@Composable
fun ListUsersScreenPreview() {
    UserExampleTheme {
        Surface {
            ListUsersScreen(state = UIState.Success(
                data = List(10) {
                    User(
                        id = it,
                        name = "User name $it",
                        email = "user@email.$it"
                    )
                }
            ))
        }
    }
}