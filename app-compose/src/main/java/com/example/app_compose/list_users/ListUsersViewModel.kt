package com.example.app_compose.list_users

import androidx.compose.runtime.getValue
import androidx.compose.runtime.mutableStateOf
import androidx.compose.runtime.setValue
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.common.UIState
import com.example.domain.User
import com.example.domain.repository.LocalRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class ListUsersViewModel @Inject constructor(
    private val userRepository: LocalRepository<User, Int>
) : ViewModel() {
    var state by mutableStateOf<UIState<List<User>>>(UIState.Idle)
        private set

    init {
        viewModelScope.launch {
            state = UIState.Loading
            runCatching {
                userRepository.getList()
                    .collectLatest {
                        state = UIState.Success(data = it)
                    }
            }.onFailure { error ->
                Timber.e(error)
                state = UIState.Error(error)
            }
        }
    }
}