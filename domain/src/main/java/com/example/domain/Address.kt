package com.example.domain

data class Address(
    val street: String = "",
    val suite: String = "",
    val city: String = "",
    val zipcode: String = "",
    val location: String = ""
) {
    override fun toString(): String {
        return "$street, $suite, $city, $zipcode"
    }
}
