package com.example.domain


data class SimpleUser(
    val id: Int,
    val name: String,
    val email: String,
)