package com.example.domain.repository

interface RemoteRepository<T, ID> {
    suspend fun getList(): List<T>

    suspend fun find(id: ID): T?

    suspend fun save(vararg item: T)
}