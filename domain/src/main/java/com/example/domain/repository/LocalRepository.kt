package com.example.domain.repository

import kotlinx.coroutines.flow.Flow

interface LocalRepository<T, ID> {
    fun getList(): Flow<List<T>>

    suspend fun count(): Int

    suspend fun find(id: ID): T?

    suspend fun save(vararg item: T)
}